#!/bin/bash
sudo apt update -y && upgrade -y
sudo apt install build-essential --fix-missing -y
sudo apt install  gcc --fix-missing  -y
sudo apt install make --fix-missing  -y
sudo apt-get install gcc-multilib --fix-missing -y
sudo apt-get install zlib1g --fix-missing -y
sudo apt-get install lib32z1-dev -y
sudo apt-get -y install libreadline-dev --fix-missing -y
sudo apt-get install ncurses-bin -y
sudo apt install libssl-dev --fix-missing -y
sudo apt install git -y
sudo git clone https://github.com/SoftEtherVPN/SoftEtherVPN_Stable.git
cd SoftEtherVPN_Stable/
sudo ./configure
sudo make
sudo make install
echo I did it
exit 0
